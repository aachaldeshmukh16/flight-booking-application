package org.app.flight.booking.Service;

import org.app.flight.booking.models.Flight;
import org.app.flight.booking.models.FlightDetails;
import org.app.flight.booking.models.Temp;
import org.app.flight.booking.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlightSearchService {

    @Autowired
    private FlightRepository flightRepository;

    public FlightSearchService(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }






    public List<Flight> getFlightsBysorrceDestination(String source, String destination , int noOfSeats) {
        List<Flight>  result  = flightRepository.allFlights();
        List<Flight> filights =result.stream().filter(flight -> flight.getSource().equals(source)&& flight.getDestination().equals(destination)).collect(Collectors.toList());
        if(!filights.isEmpty())
        {
            for(Flight fl:filights)
            {
                for (FlightDetails fd : fl.getFlightDetails())
                {
                    Integer bookeSeatsForClass =  fl.getBooked().get(fd.getFlightClass());
                    if(null != bookeSeatsForClass)
                    {
                        int avs = fd.getNoOfSeats() - bookeSeatsForClass;
                        if(avs >= noOfSeats)
                            fd.setAvailableSeats(avs);
                        else
                            fd.setAvailableSeats(0);
                    }
                    else
                        fd.setAvailableSeats(0);
                }
            }
        }
        return filights;
    }






    public List<Flight> search(String source, String destination, int noOfPassengers) {
        return new ArrayList<>();
    }

    public List<Flight> FlightSearch() {
        return new ArrayList<>();
    }

//    public List<Flight> searchBySourceDestination(String source,String destination, int noOfPassengers) {
//        return new ArrayList<>();
//    }
}



