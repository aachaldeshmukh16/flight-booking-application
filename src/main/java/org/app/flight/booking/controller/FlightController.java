package org.app.flight.booking.controller;

import org.app.flight.booking.Service.FlightSearchService;
import org.app.flight.booking.models.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FlightController {
    @Autowired
    FlightSearchService fltService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Flight> allFlights(){
        return fltService.FlightSearch();
    }

//    @RequestMapping(method = RequestMethod.GET, value = "/flights/{flightId}")
//    public List<Flight> getFlightsByID(@PathVariable int flightId){
//
//
//        return  fltService.SearchById(flightId);
//    }

    @RequestMapping(method = RequestMethod.GET, path = "/getFlightsBysorrceDestination")
    public List<Flight>  getFlightsBysorrceDestination(@PathVariable String source, @PathVariable String destination, @PathVariable int noOfSeats){
        return fltService.getFlightsBysorrceDestination(source,destination,noOfSeats);

    }

//    @RequestMapping(method = RequestMethod.GET, value = "/flights/{src}/{dest}/{nop}")
//    public List<FlightCoverage> findAllAvailalbeFlights(@PathVariable(name = "src", required = true) String source,
//                                                        @PathVariable(name = "dest", required = true) String destination, @PathVariable(name ="nop", required = false) Integer noPass)
//    {
//        if(noPass == 0)
//            noPass = 1;
//        System.out.println("Source : " + source + "Dest: "+destination);
//        List<FlightCoverage> fcs = flightService.findAllFlightsBySrcDest(source, destination, noPass);
//        return fcs;
//    }




}
