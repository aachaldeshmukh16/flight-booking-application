package org.app.flight.booking.repository;

import org.app.flight.booking.models.Flight;
import org.app.flight.booking.models.FlightDetails;
import org.app.flight.booking.models.Temp;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class FlightRepository {
    List<Flight> ListId = new ArrayList<>();

    List<Flight> sourceDestination = new ArrayList<>();

    //List<Flight> flights = new ArrayList();

    public List<Flight> allFlights(){

        List<Flight> flights = new ArrayList();
        List<FlightDetails> boiengDetails = new ArrayList<>();

        boiengDetails.add(new FlightDetails(1,"FirstClass", 8, 20000.0, 0));
        boiengDetails.add(new FlightDetails(1,"BuisnessClass", 34, 13000.0, 0));
        boiengDetails.add(new FlightDetails(1,"Economy", 195, 6000.0, 0));

        Map<String,Integer> boiengDetailsSeatsBooked = new HashMap<>();
        boiengDetailsSeatsBooked.put("FirstClass", 3 );

        List<FlightDetails> airbus1Details = new ArrayList<>();
        //airbus1Details.add(new flight_details(2,"FirstClass", 0, 0.0));
        //airbus1Details.add(new flight_details(2,"BuisnessClass", 0, 0.0));
        airbus1Details.add(new FlightDetails(2,"Economy", 144, 4000.0,0));

        List<FlightDetails> airbus21Details = new ArrayList<>();
        //airbus21Details.add(new flight_details(3,"FirstClass", 0, 0.0));
        airbus21Details.add(new FlightDetails(3,"BuisnessClass", 20, 10000.0, 0));
        airbus21Details.add(new FlightDetails(3,"Economy", 152, 5000.0, 0));


        Flight f1 = new Flight("Boieng", 1, boiengDetails,"Pune", "Nagpur");
        Flight f2 = new Flight("Airbus_A319_v2", 2, airbus1Details,"Hyd", "Del");
        Flight f3 = new Flight("Airbus_a321", 3, airbus21Details, "Hyd", "Del");

        flights.add(f1);
        flights.add(f2);
        flights.add(f3);

        return flights;
    }

//   public List<Temp> findAllFlights()
//   {
//       ArrayList<Temp> temps = new ArrayList<>();
//       temps.add(new Temp(1,"Hyd","Del"));
//       temps.add(new Temp(1,"Hyd","Del"));
//
//       return temps;
//   }
//    public List<Flight> searcgById(int flightid){
//        for (Flight fc:flights) {
//            if(fc.getFlightId()== flightid){
//                ListId.add(fc);
//            }
//        }
//        return ListId;
//    }
//
//    public List<Flight> searchBySourceDestination(String source, String destination, int noOfSeats){
//        for (Flight fc:flights) {
//            if(fc.getSource().equals(source)&& fc.getDestination().equals(destination)&&fc.getFlight_details()){
//                sourceDestination.add(fc);
//            }
//        }
//        return sourceDestination;
//    }









}
