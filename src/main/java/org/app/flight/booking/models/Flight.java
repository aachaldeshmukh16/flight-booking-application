package org.app.flight.booking.models;

import java.util.List;
import java.util.Map;

public class Flight {

    private String flightName;
    private int flightId;
    private String source;
    private String destination;
    //private int noOfSeats;
    private List<FlightDetails> FlightDetails;
    private Map<String,Integer> booked;



    public Flight(String flightName, Integer flightId, List<FlightDetails> FlightDetails, String source, String destination) {
        this.flightName = flightName;
        this.flightId = flightId;
        this.FlightDetails = FlightDetails;
        this.source = source;
        this.destination = destination;
        this.booked = booked;
    }

    public Map<String, Integer> getBooked() {
        return booked;
    }

    public void setBooked(Map<String, Integer> booked) {
        this.booked = booked;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public List<FlightDetails> getFlightDetails() {
        return FlightDetails;
    }

    public void setFlightDetails(List<FlightDetails> flightDetails) {
        this.FlightDetails = flightDetails;
    }
}
