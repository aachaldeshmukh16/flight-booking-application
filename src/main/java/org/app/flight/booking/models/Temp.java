package org.app.flight.booking.models;

public class Temp {
    private Integer flightId;
    private String source;
    private String destination;


    public Temp(Integer flightId, String source, String destination) {
        this.flightId = flightId;
        this.source = source;
        this.destination = destination;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
