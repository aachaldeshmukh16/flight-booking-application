package org.app.flight.booking.models;

public class FlightDetails {
    //private String flightName;
    private Integer flightId;
    private String flightClass;
    private Integer noOfSeats;
    private Double basePrice;
    private Integer availableSeats;



    public FlightDetails(Integer flightId, String flightClass, Integer noOfSeats, double basePrice, Integer availableSeats) {

        this.flightId = flightId;
        this.flightClass = flightClass;
        this.noOfSeats = noOfSeats;
        this.basePrice = basePrice;
        this.availableSeats = availableSeats;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }



    public Integer getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(Integer noOfSeats) {
        noOfSeats = noOfSeats;
    }

//    public String getFlightName() {
//        return flightName;
//    }
//
//    public void setFlightName(String flightName) {
//        this.flightName = flightName;
//    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(Integer availableSeats) {
        this.availableSeats = availableSeats;
    }
}
