package org.app.flight.booking.Service;

import org.app.flight.booking.models.Flight;
import org.app.flight.booking.models.FlightDetails;
import org.app.flight.booking.repository.FlightRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FlightSearchServiceTest
{
      @Mock
      private FlightRepository flightRepository;

      @InjectMocks
      FlightSearchService flightSearchService;

    @Test
    public void shouldReturnEmptyFlightListWhenNoSourceEntered() {

        FlightSearchService flightSearchService = new FlightSearchService(flightRepository);
        List<Flight> flights = flightSearchService.search("", "", 0);
        assertEquals(0, flights.size());
    }

    @Test
    public void shouldReturnFlightListWithSourceDestination(){
        List<Flight> allFlightsList = allFlights();
        when(flightRepository.allFlights()).thenReturn(allFlightsList);
        ArrayList<Flight> expected = findExpectedFlightsCoverage();
        List<Flight> result = flightSearchService.getFlightsBysorrceDestination("HYD", "DEL", 1);
        Assert.assertEquals(1,expected.size());
    }

    private ArrayList<Flight> findExpectedFlightsCoverage()
    {
        ArrayList<Flight> flightCoverage = new ArrayList<>();

        ArrayList<FlightDetails> boeingDetails = new ArrayList<>();
        FlightDetails fc = new FlightDetails(1,"FirstClass",8,20000.0,8);
        fc.setAvailableSeats(3);
        boeingDetails.add(fc);
        FlightDetails bc = new FlightDetails(1,"BuisnessClass",34,13000.0,20);
        bc.setAvailableSeats(4);
        boeingDetails.add(bc);
        FlightDetails ec = new FlightDetails(1,"Economy",195,6000.0,10);
        ec.setAvailableSeats(40);
        boeingDetails.add(ec);

        List<FlightDetails> airbus1Details = new ArrayList<>();
        FlightDetails bc1 = new FlightDetails(2,"Economy",144,4000.0,12);
        bc1.setAvailableSeats(15);
        airbus1Details.add(bc1);



        HashMap<String, Integer> bookedSeats1 = new HashMap<>();
        bookedSeats1.put("FC", 5);
        bookedSeats1.put("BC", 30);
        bookedSeats1.put("EC", 155);
        Flight flc1 = new Flight("Boieng",1,boeingDetails,"HYD","DEL");

        flightCoverage.add(flc1);

      /*  HashMap<String, Integer> bookedSeats10 = new HashMap<>();
        bookedSeats10.put("EC", 110);
        bookedSeats10.put("BC", 5);
        Flight flc10 = new Flight("Airbus_A319_v2",2,airbus1Details,"Pune","Nagpur");
        flightCoverage.add(flc10);
*/
        return flightCoverage;
    }





    private List<Flight> allFlights(){

        List<Flight> flights = new ArrayList<>();

        List<FlightDetails> boiengDetails = new ArrayList<>();

        boiengDetails.add(new FlightDetails(1,"FirstClass", 8, 20000.0, 0));
        boiengDetails.add(new FlightDetails(1,"BuisnessClass", 34, 13000.0, 0));
        boiengDetails.add(new FlightDetails(1,"Economy", 195, 6000.0, 0));

        Map<String,Integer> boiengDetailsSeatsBooked = new HashMap<>();
        boiengDetailsSeatsBooked.put("FirstClass", 3 );

        List<FlightDetails> airbus1Details = new ArrayList<>();
        //airbus1Details.add(new flight_details(2,"FirstClass", 0, 0.0));
        //airbus1Details.add(new flight_details(2,"BuisnessClass", 0, 0.0));
        airbus1Details.add(new FlightDetails(2,"Economy", 144, 4000.0,0));

        List<FlightDetails> airbus21Details = new ArrayList<>();
        //airbus21Details.add(new flight_details(3,"FirstClass", 0, 0.0));
        airbus21Details.add(new FlightDetails(3,"BuisnessClass", 20, 10000.0, 0));
        airbus21Details.add(new FlightDetails(3,"Economy", 152, 5000.0, 0));


        Flight f1 = new Flight("Boieng", 1, boiengDetails,"Pune", "Nagpur");
        Flight f2 = new Flight("Airbus_A319_v2", 2, airbus1Details,"Hyd", "Del");
        Flight f3 = new Flight("Airbus_a321", 3, airbus21Details, "Hyd", "Del");

        flights.add(f1);
        flights.add(f2);
        flights.add(f3);

        return flights;
    }


//    @Test
//    public void withSourceDestination(){
//        ArrayList<Temp> temps = new ArrayList<>();
//        temps.add(new Temp(1,"Hyd","Del"));
//        temps.add(new Temp(1,"Hyd","Del"));
//
//        when(obj.findAllFlights()).thenReturn(temps);
//
//        FlightSearchService flightSearchService = new FlightSearchService(obj);
//        List<Temp> filteredFlights = flightSearchService.search("Hyd","Del",)
//
//    }
}